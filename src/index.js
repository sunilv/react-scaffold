import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Grommet } from 'grommet';

import { grommet } from 'grommet/themes';
import { deepMerge } from 'grommet/utils';
import { THEME } from './styles';

import Protected from './utils/Protected';
import NotFound from './pages/PageNotFound';
import Home from './pages/Home';
import Accounts from './pages/Accounts';
import Help from './pages/Help';
import DataControlCenter from './pages/DataControlCenter';
import ForgetPassword from './pages/ForgetPassword';
import Login from './pages/Login';

const customTheme = deepMerge(grommet, THEME.PROPS);

const Main = () => (
  <Grommet full theme={customTheme}>
    <Router>
      <Switch>
        <Protected path="/" exact component={Home} />
        <Protected path="/account-settings" exact component={Accounts} />
        <Protected path="/data-control-center" exact component={DataControlCenter} />
        <Protected path="/help-and-support" exact component={Help} />
        <Route path="/forgot-password" exact component={ForgetPassword} />
        <Route path="/auth" exact component={Login} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Router>
  </Grommet>
);

ReactDOM.render(<Main />, document.getElementById('root'));
