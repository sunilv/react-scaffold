import React, { Component, createRef } from 'react';
import { Box, Heading, Anchor } from 'grommet';

import { withRouter } from 'react-router-dom';

class Home extends Component {
  constructor(props) {
    super(props);
    this.searchBoxRef = createRef();
    this.state = {
      loading: true,
      index: 0,
    };
  }

  onActive = nextIndex => {
    this.setState({
      index: nextIndex,
    });
  };

  componentDidMount() {}

  render() {
    const { index } = this.state;
    return (
      <Box height={'100vh'} pad="medium">
        <Heading level="2">Wellcome to SaturnQ's React Scaffold for quick startup project.</Heading>
        <Heading level="4">
          Click{' '}
          <Anchor label="here" href="https://gitlab.com/sunilv/react-scaffold" target="_blank" />{' '}
          for React documentation, tips and coding style.
        </Heading>
        <Heading level="5">Black and Orange: Lively and Powerful Theme </Heading>
        <Box>
          <Heading level="5">Available modules</Heading>
          <Anchor label="Top Nav Bar" href="#" />
          <Anchor label="Error Page (404,500 and unknown errors)" href="/er" />
          <Anchor label="Data Controll Center" href="/data-control-center" />
          <Anchor label="Account-settings" href="/account-settings" />
          <Anchor label="Forget Password" href="/forgot-password" />
          <Anchor label="Login and Sign Up" href="/auth" />
        </Box>
      </Box>
    );
  }
}

export default withRouter(Home);
