import React, { Component, createRef } from 'react';
import { Box, Image, Form, Heading, TextInput, Paragraph, Button } from 'grommet';
import styled from 'styled-components';
import { COLORS } from '../styles';
import { Search, CircleQuestion, Support, ContactInfo } from 'grommet-icons';

const CardWrapper = styled.div`
  margin: 10px;
`;

const Description = styled.span`
  font-size: 0.5em;
`;

const IconWrapper = styled.div`
  padding: 10px;
`;
const SearchIconWrapper = styled.div`
  background-color: #f9d342;
  height: 25px;
  width: 25px;
  border-radius: 8px;
  padding: 5px;
  alignitems: 'center';
  display: 'flex';
  margin-right: 3px;
`;

class Help extends Component {
  constructor(props) {
    super(props);
    this.searchBoxRef = createRef();
    this.state = {
      loading: true,
      index: 0,
    };
  }

  onActive = nextIndex => {
    alert(nextIndex);
    this.setState({
      index: nextIndex,
    });
  };

  componentDidMount() {}

  render() {
    const { suggestionOpen, locationValue } = this.state;
    return (
      <Box
        align="center"
        background="white"
        pad="medium"
        width={`${window.innerWidth}px`}
        height={`${window.innerHeight}px`}
        margin="large"
      >
        <Heading level={2}>How Can We Help You?</Heading>
        <Box
          ref={this.searchBoxRef}
          width="large"
          background="background"
          direction="row"
          align="center"
          pad="none"
          elevation={suggestionOpen ? 'medium' : undefined}
          style={{
            // borderRightColor: COLORS.tertiary,
            // borderWidth: '1px',
            // borderRightStyle: 'solid',
            borderRadius: '10px',
          }}
        >
          <TextInput
            type="search"
            dropTarget={this.searchBoxRef.current}
            plain
            style={{
              fontSize: '0.8em',
            }}
            // value={value}
            // onChange={onChange}
            // onSelect={onSelect}
            // suggestions={renderSuggestions()}
            placeholder="Search Topics.."
            // onSuggestionsOpen={() => setSuggestionOpen(true)}
            // onSuggestionsClose={() => setSuggestionOpen(false)}
          />
          <SearchIconWrapper>
            <Search color="white" size="medium" />
          </SearchIconWrapper>
        </Box>
        <Box direction="row" margin="medium" pad="medium" width="800px">
          {[...Array(5).keys()].map((card, key) => {
            return (
              <CardWrapper>
                <Box
                  width={400}
                  background="background"
                  pad="small"
                  style={{
                    borderRadius: '10px',
                  }}
                >
                  <IconWrapper>
                    <Support color="brand" size="medium" />
                  </IconWrapper>
                  <Box>
                    <Paragraph size="small">FAQ?</Paragraph>
                    <Button label="See More.." size="xsmall" onClick={() => {}} color="white" />
                  </Box>
                </Box>
              </CardWrapper>
            );
          })}
        </Box>
      </Box>
    );
  }
}

export default Help;
