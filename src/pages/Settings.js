import React, { Component, createRef } from 'react';
import { Box, Image, Form, Heading, TextInput, Paragraph, Button } from 'grommet';

import styled from 'styled-components';
import { COLORS } from '../styles';

const Description = styled.span`
  font-size: 0.7em;
`;

class Settings extends Component {
  constructor(props) {
    super(props);
    this.searchBoxRef = createRef();
    this.state = {
      loading: true,
      index: 0,
    };
  }

  onActive = nextIndex => {
    alert(nextIndex);
    this.setState({
      index: nextIndex,
    });
  };

  componentDidMount() {}

  render() {
    return (
      <Box
        background="white"
        pad="medium"
        width={`${window.innerWidth}px`}
        // height={`${window.innerHeight}px`}
      >
        <Heading level={4} margin="none">
          Enter Old and New Passwords <br />
          <Description>
            Hint: Password should contain 1) One Upper case 2) One lower case 3) minimum 8
            characters
          </Description>
        </Heading>
        <Box
          width="30%"
          justify="around"
          gap="medium"
          margin={{
            vertical: 'small',
          }}
        >
          <TextInput
            placeholder="Old Password"
            onChange={() => {}}
            size="small"
            plain
            type="password"
            style={{
              backgroundColor: COLORS.background,
            }}
          />
          <TextInput
            placeholder="New Password"
            onChange={() => {}}
            size="small"
            size="small"
            plain
            type="password"
            style={{
              backgroundColor: COLORS.background,
            }}
          />
          <TextInput
            placeholder="New Password Again"
            onChange={() => {}}
            size="small"
            size="small"
            plain
            type="password"
            style={{
              backgroundColor: COLORS.background,
            }}
          />

          <Button type="button" size="medium" label="Change Password" primary />
        </Box>
        <Box direction="row">
          <Paragraph size="small">
            Forgot your password? Reset <a href="/forgot-password">here</a>{' '}
          </Paragraph>
        </Box>
      </Box>
    );
  }
}

export default Settings;
