import React from 'react';

import { Anchor, Box, Heading, Paragraph, TextInput, Button } from 'grommet';
import { Next, LinkPrevious, FormUp } from 'grommet-icons';
import { COLORS } from '../styles';

import { withRouter } from 'react-router-dom';

const ForgetPassword = props => {
  return (
    <Box fill align="center" justify="center">
      <Box
        margin="small"
        direction="row"
        justify="center"
        gap="small"
        style={{
          position: 'absolute',
          top: 10,
          left: 10,
        }}
      >
        <LinkPrevious color={COLORS.secondary} />
        <Anchor label="Back to Home" href="/" color={COLORS.secondary} />
      </Box>
      <Heading>Forgot Password?</Heading>
      <Paragraph textAlign="center">
        Please enter Email or Mobile number. OTP will be send for verification.
      </Paragraph>
      <Box width={'30%'} gap="large">
        <TextInput
          placeholder="Email or Mobile Number"
          onChange={() => {}}
          size="small"
          size="small"
          plain
          style={{
            backgroundColor: COLORS.white,
          }}
        />

        <Button primary icon={<Next />} reverse label="Next" onClick={() => {}} size="small" />
      </Box>
      <Box>
        <Paragraph>
          Having Trouble? Contact{' '}
          <Anchor label="Help & Support" href="/help-and-support" target="_blank" />.
        </Paragraph>
      </Box>
    </Box>
  );
};

export default withRouter(ForgetPassword);
