import React, { useState } from 'react';

import {
  Anchor,
  Box,
  Button,
  Heading,
  Paragraph,
  TextInput,
  Grommet,
  Tab,
  Tabs,
  Footer,
  CheckBox,
} from 'grommet';
import { Next, LinkPrevious, FormUp } from 'grommet-icons';
import styled, { css } from 'styled-components';
import { grommet } from 'grommet/themes';
import { deepMerge } from 'grommet/utils';
import { COLORS } from '../styles';

import { withRouter } from 'react-router-dom';

const customTabTheme = deepMerge(grommet, {
  tab: {
    margin: undefined,
    pad: {
      bottom: undefined,
      horizontal: 'small',
    },
  },
  tabs: {
    gap: 'small',
    background: COLORS.background,

    header: {
      background: COLORS.background,
      extend: ({ theme }) => css`
        padding: ${theme.global.edgeSize.small};
      `,
    },
    panel: {
      extend: ({ theme }) => css`
        background: ${COLORS.background};
        padding: ${theme.global.edgeSize.small};
      `,
    },
  },
  table: {
    header: {
      border: undefined,
    },
  },
  text: {
    medium: {
      size: '14px',
    },
  },
});

const Login = props => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [checked, setChecked] = useState(!!props.checkedProp);
  const onChange = event => setChecked(event.target.checked);
  return (
    <Box background="white" fill justify="center" align="center">
      <Box
        margin="small"
        direction="row"
        justify="center"
        gap="small"
        style={{
          position: 'absolute',
          top: 10,
          left: 10,
        }}
      >
        <LinkPrevious color={COLORS.secondary} />
        <Anchor label="Back to Home" href="/" color={COLORS.secondary} />
      </Box>
      <Heading>Sign In </Heading>

      <Paragraph textAlign="center">
        Hello there! Sign in and start managing your Account!
      </Paragraph>

      <Grommet theme={customTabTheme}>
        <Tabs activeIndex={activeIndex} onActive={setActiveIndex}>
          {['Login', 'Sign Up'].map((item, key) => {
            return (
              <Tab title={item} key={`tab-bar-${key}`}>
                {key ? (
                  <Box
                    pad="small"
                    margin="medium"
                    gap="medium"
                    width="500px"
                    height="350px"
                    background={COLORS.background}
                  >
                    <TextInput
                      placeholder="Email or Mobile Number"
                      onChange={() => {}}
                      size="small"
                      plain
                      style={{
                        backgroundColor: COLORS.white,
                      }}
                    />
                    <TextInput
                      placeholder="Password"
                      onChange={() => {}}
                      size="small"
                      type="password"
                      plain
                      style={{
                        backgroundColor: COLORS.white,
                      }}
                    />
                    <TextInput
                      placeholder="Password"
                      onChange={() => {}}
                      size="small"
                      type="password"
                      plain
                      style={{
                        backgroundColor: COLORS.white,
                      }}
                    />
                    <CheckBox
                      checked={checked}
                      onChange={onChange}
                      size="small"
                      label={
                        <Paragraph size="small">
                          By Signing up, You are agreeing to our{' '}
                          <a href="/forgot-password">Terms & Conditions</a> and{' '}
                          <a href="/forgot-password">Privacy Policy</a>
                        </Paragraph>
                      }
                    />
                    <Button
                      primary
                      icon={<Next />}
                      reverse
                      label="Sign Up"
                      onClick={() => {}}
                      size="small"
                    />
                  </Box>
                ) : (
                  <Box pad="small" margin="medium" gap="medium" width="500px" height="350px">
                    <TextInput
                      placeholder="Email or Mobile Number"
                      onChange={() => {}}
                      size="small"
                      plain
                      style={{
                        backgroundColor: COLORS.white,
                      }}
                    />
                    <TextInput
                      placeholder="Password"
                      onChange={() => {}}
                      size="small"
                      type="password"
                      plain
                      style={{
                        backgroundColor: COLORS.white,
                      }}
                    />
                    <Button
                      primary
                      icon={<Next />}
                      reverse
                      label="Login"
                      onClick={() => {}}
                      size="small"
                    />

                    <Paragraph size="small">
                      Forgot your password? Reset <a href="/forgot-password">here</a>{' '}
                    </Paragraph>
                  </Box>
                )}
              </Tab>
            );
          })}
        </Tabs>
      </Grommet>
    </Box>
  );
};

export default withRouter(Login);
