import React, { Component, createRef } from 'react';
import { Box, Image, Anchor, Tab, Tabs, Grommet } from 'grommet';
import { withRouter } from 'react-router-dom';
import { MENU } from '../constants';
import styled, { css } from 'styled-components';
import { grommet } from 'grommet/themes';
import { deepMerge } from 'grommet/utils';
import UserList from '../components/TableView';
import { User, LinkPrevious } from 'grommet-icons';

import { COLORS } from '../styles';

const customTabTheme = deepMerge(grommet, {
  tab: {
    margin: undefined,
    pad: {
      bottom: undefined,
      horizontal: 'small',
    },
  },
  tabs: {
    gap: 'small',
    header: {
      background: COLORS.background,
      extend: ({ theme }) => css`
        padding: ${theme.global.edgeSize.small};
      `,
    },
    panel: {
      extend: ({ theme }) => css`
        background: ${COLORS.white};
        padding: ${theme.global.edgeSize.small};
      `,
    },
  },
  table: {
    header: {
      border: undefined,
    },
  },
  text: {
    medium: {
      size: '14px',
    },
  },
});

const TAB_COMPONENTS = {
  'user-list': UserList,
  'help-and-support': UserList,
};

const TabWrapper = styled.div`
  margin-top: 0px;
`;

class Accounts extends Component {
  constructor(props) {
    super(props);
    this.searchBoxRef = createRef();
    this.state = {
      loading: true,
      index: 0,
    };
  }

  onActive = nextIndex => {
    this.setState({
      index: nextIndex,
    });
  };

  componentDidMount() {}

  render() {
    const { index } = this.state;
    return (
      <Box background="white" pad={0} height="100vh">
        <TabWrapper>
          <Grommet theme={customTabTheme}>
            <Tabs activeIndex={index} onActive={this.onActive}>
              {MENU.adminOptions.map((item, key) => {
                const TabComponent = TAB_COMPONENTS[item.component];
                return (
                  <Tab title={item.label} key={`tab-bar-${key}`}>
                    <Box pad={0}>{<TabComponent />}</Box>
                  </Tab>
                );
              })}
            </Tabs>
          </Grommet>
        </TabWrapper>
      </Box>
    );
  }
}

export default withRouter(Accounts);
