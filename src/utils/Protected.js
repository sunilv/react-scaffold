/* eslint-disable no-unused-vars */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import App from '../App';

const Protected = ({ component: Component, ...rest }) => {
  const token = true;
  if (token) {
    return (
      <Route
        render={() => (
          <App>
            <Component {...rest} />
          </App>
        )}
      />
    );
  }
  return <Redirect to={{ pathname: '/login', state: { from: rest.location.pathname } }} />;
};

export default Protected;
