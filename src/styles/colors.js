export const primary = '#c5283d';
export const secondary = '#0a0908';
export const tertiary = '#383d3b';
export const quartenary = '#eee5e9';
export const background = '#fafdf6';
export const backgrounder = '#fafafc';
export const light = '#f7f0f5';

// 9b9baa
export const white = '#fff';
export const black = '#040403';
