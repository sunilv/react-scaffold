import React from 'react';

import { Anchor, Box, Header, Nav, Image, Text, Menu } from 'grommet';
import { FormDown, Notification, FormUp } from 'grommet-icons';
import { withRouter } from 'react-router-dom';

const items = [
  { label: 'Dashboard', href: '#' },
  { label: 'Shopping', href: '#' },
  { label: 'Status', href: '#' },
  { label: 'Users', href: '#' },
];

const items1 = [{ label: 'Data Control Center', href: '/data-control-center' }];

const TopBar = props => {
  return (
    <Header
      background="secondary"
      pad="small"
      style={{
        position: 'fixed',
        left: '57px',
        right: 0,
      }}
    >
      <Nav direction="row">
        {items.map(item => (
          <Anchor href={item.href} label={item.label} key={item.label} color="brand" />
        ))}
      </Nav>
      <Box direction="row" align="center" gap="small">
        <Nav direction="row">
          {items1.map(item => (
            <Anchor href={item.href} label={item.label} key={item.label} color="white" />
          ))}
        </Nav>
        <Notification size="medium" color="white" />
        <Menu
          plain
          icon={false}
          items={[
            {
              label: 'Account Setting',
              onClick: () => {
                props.history.push('/account-settings');
              },
            },
            {
              label: 'Help and Support',
              onClick: () => {
                props.history.push('/help-and-support');
              },
            },
            {
              label: 'Logout',
              onClick: () => {
                props.history.push('/auth');
              },
            },
          ]}
        >
          {({ drop, hover }) => {
            return (
              <Box
                direction="row"
                gap="small"
                pad={drop ? 'small' : undefined}
                align="center"
                justify="center"
                background={hover && drop ? 'light-2' : undefined}
              >
                <Box
                  height="30px"
                  width="30px"
                  border
                  style={{
                    borderRadius: '10px',
                  }}
                >
                  <Image
                    src="https://randomuser.me/api/portraits/men/22.jpg"
                    fit="contain"
                    style={{
                      borderRadius: '10px',
                    }}
                  />
                </Box>
                {drop ? 'Hi Barry,' : null}
                {drop ? <FormUp color={'black'} /> : <FormDown color={'white'} />}
              </Box>
            );
          }}
        </Menu>
      </Box>
    </Header>
  );
};

export default withRouter(TopBar);
