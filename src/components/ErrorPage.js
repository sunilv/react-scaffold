import * as React from 'react';
import { Anchor, Box, Heading } from 'grommet';
import { CircleInformation } from 'grommet-icons';
import PropTypes from 'prop-types';

const messages = {
  404: 'Sorry, Page Not Found',
  500: 'There was error in Request. Please try again or contact Help & Support',
  unknown: 'Unknown Issue.Contact Help & Support',
};

const ErrorPage = ({ code }) => (
  <Box fill align="center" justify="center">
    <CircleInformation size="large" />
    <Heading level={2}>{messages[code]}</Heading>
    <Anchor href="/" label="Back to home" size="medium" />
  </Box>
);

ErrorPage.propTypes = {
  code: PropTypes.oneOf(['404', '500', 'Unknown']),
};

ErrorPage.defaultProps = {
  code: '404',
};
export default ErrorPage;
