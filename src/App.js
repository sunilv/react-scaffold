import React from 'react';
import { Box, Text } from 'grommet';

import TopBar from './components/TopBar';

function App(props) {
  const currentURl = props.children.props.path;
  return (
    <div>
      {['/data-control-center'].indexOf(currentURl) > -1 ? (
        <Box direction="row" pad={0} background="white">
          {props.children}
        </Box>
      ) : (
        <Box direction="row" pad={0} background="white">
          <Box align="center" justify="center" width={'60px'} pad={0} background="brand">
            <Text color="secondary">Side bar</Text>
          </Box>
          <Box
            width="100%"
            height={`100vh`}
            pad={0}
            style={{
              overflow: 'scroll',
            }}
          >
            <TopBar currentUrl={currentURl} />
            {props.children}
          </Box>
        </Box>
      )}
    </div>
  );
}

export default App;
