// Side bar Menu

export const sideBar = [
  {
    label: 'Home',
    url: '/',
  },

  {
    label: 'Search Job',
    url: '/search',
  },
  {
    label: 'For Companies',
    url: '/company',
  },
  {
    label: 'Community',
    url: '/community',
  },
  {
    label: 'More..',
    url: '/me',
  },
];

export const topBar = [
  {
    label: 'My Profile',
    component: 'change-password',
  },
  {
    label: 'Change Password',
    component: 'change-password',
  },
  {
    label: 'Privacy Setting',
    component: 'change-password',
  },
  {
    label: 'Notifications',
    component: 'change-password',
  },
  {
    label: 'Mobile',
    component: 'change-password',
  },
];

export const adminOptions = [
  {
    label: 'Manage Users',
    component: 'user-list',
  },
  {
    label: 'Manage Roles',
    component: 'user-list',
  },
  {
    label: 'Manage Applications',
    component: 'user-list',
  },
  {
    label: 'Manage Other Data',
    component: 'user-list',
  },
  {
    label: 'Manage Other Data',
    component: 'user-list',
  },
];
