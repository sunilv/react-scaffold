import * as Urls from './urls';
import * as MENU from './menu';
import * as ROLES from './role';

export { Urls, MENU, ROLES };
