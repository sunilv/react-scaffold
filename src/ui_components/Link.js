import React from 'react';
import styled from 'styled-components';
import { COLORS } from '../styles';
import { Link as A } from 'react-router-dom';
import PropTypes from 'prop-types';

const LinkWrapper = styled.div`
  border-left-color: ${props => (props.active ? COLORS.secondary : COLORS.white)};
  border-left-style: solid;
  padding: 1px 15px;
  a {
    border-left-color: ${props => (props.active ? COLORS.base : COLORS.unselect)};
    border-left-width: '1';
    color: ${props => (props.active ? COLORS.base : COLORS.unselect)};
    font-size: 0.8em;
    font-family: 'Poppins';
    text-decoration: none;
    &:hover {
      color: ${COLORS.base};
    }
  }
`;

function Link({ isActiveUrl, to, label }) {
  console.log(to, 'd');
  return (
    <LinkWrapper active={isActiveUrl}>
      <A to={to}> {label}</A>
    </LinkWrapper>
  );
}

Link.propTypes = {
  isActiveUrl: PropTypes.bool,
};

export default Link;
